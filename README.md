# Temperature forecasting Git Repository

This temperature forecasting tool is a Python Dash app based on Dash and Plotly packages. This is designed to help with forecasting temperatures for Australian cities and towns within the energy market. It displays variables (surface based and upper-level) for several global NWP models. The data is accessed via the Weatherzone API.

Python file: app.py