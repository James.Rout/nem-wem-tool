from datetime import datetime, timedelta
import metpy.calc as mpcalc
from metpy.units import units
import numpy as np
import pandas as pd
import pytz
import requests
import plotly.graph_objs as go
import dash
from dash import html, dcc
from dash.dependencies import Input, Output, State

def pick_location(location):
    ### Get weatherzone api urls for each location
    ###
    if location =='Sydney':
        loc = 66214
        ac_name='ACCESSC_SY_APS3'
    elif location=='Melbourne':
        loc=86338
        ac_name='ACCESSC_VT_APS3'
    elif location=='Adelaide':
        loc=23000
        ac_name='ACCESSC_AD_APS3'
    elif location=='Brisbane':
        loc=40913
        ac_name='ACCESSC_BN_APS3'
    elif location=='Canberra':
        loc = 70351
        ac_name='ACCESSC_SY_APS3'
    elif location=='Bankstown':
        loc=66137
        ac_name='ACCESSC_SY_APS3'
    elif location =='Perth':
        loc=9225
        ac_name='ACCESSC_PH_APS3'
    elif location=='Geraldton':
        loc=8315
        ac_name='ACCESSC_PH_APS3'
    elif location=='Bunbury':
        loc=9965
        ac_name='ACCESSC_PH_APS3'
    elif location=='Albany':
        loc=9999
        ac_name='ACCESSC_PH_APS3'
    elif location=='Kalgoorlie':
        loc=12038
        ac_name='ACCESSC_PH_APS3'

        
    url_ecmwf=f'https://o5.wxapi.com.au/element-items/location-grid-point-grid-point/SITE/{loc}/LOC_FORECAST/NEAREST_LS/ECMWF_HRES/'
    url_gfs=f'https://o5.wxapi.com.au/element-items/location-grid-point-grid-point/SITE/{loc}/LOC_FORECAST/NEAREST_LS/GFS_0P25/'
    url_ac_g=f'https://o5.wxapi.com.au/element-items/location-grid-point-grid-point/SITE/{loc}/LOC_FORECAST/NEAREST_LS/ACCESSG_APS3/'
    url_ac_c=f'https://o5.wxapi.com.au/element-items/location-grid-point-grid-point/SITE/{loc}/LOC_OBSERVATIONS/NEAREST_LS/{ac_name}/'
    url_wrf = f'https://o5.wxapi.com.au/element-items/location-grid-point-grid-point/SITE/{loc}/LOC_OBSERVATIONS/NEAREST_LS/WRF_WZ_AU/'
    
    return url_ecmwf, url_gfs, url_ac_g, url_ac_c, url_wrf

def select_model(url):
    ###Read json file from url and return json along with a dataframe
    ###
    json=pd.read_json(url)['body']
    df=pd.DataFrame(json[0])[['values']].transpose()
    
    #Get timestamps
    
    time=[]
    time.append(json[0]['timestamp'])
    
    for i in range(1,len(json)):
        time.append(json[i]['timestamp'])
        df_i=pd.DataFrame(json[i])[['values']].transpose()
        df=pd.concat([df,df_i])
    
    issue_date=pd.to_datetime(json[0]['issued']).date()
    issue_time=pd.to_datetime(json[0]['issued']).time()
    model=json[0]['sourceCode']
    model=model.split('_')[0]
    
    df.index=pd.to_datetime(time)
    
    new_df=df.copy()
    new_df['WIND_SPEED_kmh.H10M']=df['WIND_SPEED.H10M']*3.6
    new_df['Dewpoint_depression.H2M']=(df['TEMPERATURE.H2M']-df['DEW_POINT.H2M'])

    temp_850=df['TEMPERATURE.P850HPA']
    rh_850=df['RELATIVE_HUMIDITY.P850HPA'].astype('float').interpolate()
    length_850=range(0,len(rh_850))
    Td850=[mpcalc.dewpoint_from_relative_humidity(temp_850.iloc[i]*units.degC, rh_850.iloc[i]*units.percent).m for i in length_850]*units.degC
    new_df['DEWPOINT.P850HPA']=Td850

    df=new_df
    
    return json, df, issue_date, issue_time, model

def start_time(df, start, local_time):
    df.index=df.index.tz_convert(f'Australia/{local_time}')
    df=df.loc[start::]
    return df

def localise_tz(local_time, df_ec, df_gfs, df_acg, df_wrf, df_acc_c):
    start_day=datetime.now().date()
    local_tz=pytz.timezone(f'Australia/{local_time}')
    start=datetime(start_day.year, start_day.month,start_day.day,9).astimezone(local_tz)
    
    df_ec=start_time(df_ec, start, local_time)
    df_gfs=start_time(df_gfs, start, local_time)
    df_acg=start_time(df_acg, start, local_time)
    df_wrf=start_time(df_wrf, start, local_time)
    df_acc_c=start_time(df_acc_c, start, local_time)
    
    return df_ec, df_gfs, df_acg, df_wrf, df_acc_c

def localise_time(df_ec, df_gfs,df_acg, df_acc_c, df_wrf, location):
    ### Convert timezone to local time and start the time series at 9am local time
    ###

    if location == 'Brisbane':
        local_time='Brisbane'
    elif location == 'Adelaide':
        local_time='Adelaide'
    elif location in ['Sydney','Melbourne', 'Bankstown', 'Canberra']:
        local_time='Sydney'
    else:
        local_time='Perth'

    df_ec, df_gfs, df_acg, df_wrf, df_acc_c = localise_tz(local_time, df_ec, df_gfs, df_acg, df_wrf, df_acc_c)
    
    return df_ec, df_gfs,df_acg, df_acc_c, df_wrf

def main(location):
    ### Get the dataframes for each location
    ###
    url_ecmwf, url_gfs, url_ac_g, url_acc_c, url_wrf = pick_location(location)

    json_ec, df_ec, issue_date_ec, issue_time_ec, model_ec=select_model(url_ecmwf)
    json_gfs, df_gfs, issue_date_gfs, issue_time_gfs, model_gfs=select_model(url_gfs)
    json_acg, df_acg,issue_date_acg, issue_time_acg, model_acg=select_model(url_ac_g)
    json_acc_c, df_acc_c,issue_date_acc_c, issue_time_acc_c, model_acc_c=select_model(url_acc_c)
    json_wrf, df_wrf,issue_date_wrf, issue_time_wrf, model_wrf=select_model(url_wrf)
    
    run_date= 'ECMWF run date: ' + str(issue_date_ec) + ' and time: ' + str(issue_time_ec) +' UTC'
    run_date+='\nGFS run date: ' + str(issue_date_gfs) + ' and time: ' + str(issue_time_gfs) + ' UTC'
    run_date+='\nACCESS-G run date: ' + str(issue_date_acg) + ' and time: ' + str(issue_time_acg) + ' UTC'
    run_date+='\nACCESS-C run date: ' + str(issue_date_acc_c) + ' and time: ' + str(issue_time_acc_c) + ' UTC'
    run_date+='\nWRF run date: ' + str(issue_date_wrf) + ' and time: ' + str(issue_time_wrf) + ' UTC'

    df_ec, df_gfs,df_acg, df_acc_c, df_wrf = localise_time(df_ec, df_gfs,df_acg,df_acc_c,df_wrf, location)
    
    return df_ec, df_gfs, df_acg, df_acc_c, df_wrf, run_date


df_ec_a, df_gfs_a, df_acg_a, df_acc_a, df_wrf_a, run_date=main('Adelaide')
df_ec_m, df_gfs_m, df_acg_m,df_acc_m,  df_wrf_m, run_date=main('Melbourne')
df_ec_s, df_gfs_s, df_acg_s,df_acc_s,  df_wrf_s, run_date=main('Sydney')
df_ec_b, df_gfs_b, df_acg_b, df_acc_b, df_wrf_b, run_date=main('Bankstown')
df_ec_c, df_gfs_c, df_acg_c, df_acc_c, df_wrf_c, run_date=main('Canberra')
df_ec_br, df_gfs_br, df_acg_br, df_acc_br,  df_wrf_br, run_date=main('Brisbane')

df_ec_p, df_gfs_p, df_acg_p, df_acc_p,  df_wrf_p, run_date=main('Perth')
df_ec_g, df_gfs_g, df_acg_g, df_acc_g,df_wrf_g, run_date=main('Geraldton')
df_ec_bu, df_gfs_bu, df_acg_bu, df_acc_bu, df_wrf_bu, run_date=main('Bunbury')
df_ec_al, df_gfs_al, df_acg_al, df_acc_al, df_wrf_al, run_date=main('Albany')
df_ec_k, df_gfs_k, df_acg_k, df_acc_k, df_wrf_k, run_date=main('Kalgoorlie')

#Create a table of critical temperature values

nem_and_wem=['Adelaide','Melbourne','Sydney', 'Bankstown', 'Canberra', 'Brisbane','Perth','Geraldton','Bunbury','Albany','Kalgoorlie']

hmax=[35, 35, 35, 35, 35, 35, 35, 37, 35, 30, 37]
hmin=[24, 22, 23, 22, 19, 24, 23, 23, 21, 20, 23  ]
cmax=[13, 12, 14, 14,10,16, 14, 15, 14, 12,14 ]
cmin=[5, 4, 6, 3, -2, 7, 3, 5, 3, 4,3]

hmax850=[20, 20,22, 22, 22, 21, 20, 22, 20, 18, 23]
cmax850=[0, -1,3, 3, -1, 7, 2, 6,2, -1, 1 ]
hmin850=[22,19, 22,  22, 18, 18, 21, 22, 20, 20, 23]
cmin850=[-8, -10, -10, -10, -6, -15, -14, -18,-14, -8,  -5]

crit_values=pd.DataFrame({'H_max':hmax, 'H_min':hmin, 'C_max':cmax, 'C_min':cmin, '850_hmax':hmax850, '850_hmin':hmin850, '850_cmax':cmax850, '850_cmin':cmin850}, index=nem_and_wem)

ec_date, gfs_date, ag_date, acc_date, wrf_date=run_date.split('\n')

sfc_var=[ 'TEMPERATURE_MAX_DAY.H2M', 'TEMPERATURE_MIN_DAY.H2M','TEMPERATURE.H2M', 'DEW_POINT.H2M', 'PRESSURE.MSL', 'WIND_SPEED_kmh.H10M',
         'WIND_DIRECTION.H10M','Dewpoint_depression.H2M','BOUNDARY_LAYER_HEIGHT.SFC', 'PRECIPITATION_CUMULATIVE.SFC', 'PRECIPITATION_3H.SFC']

up_var=['TEMPERATURE.P850HPA', 'DEWPOINT.P850HPA','TEMPERATURE.P500HPA', 'GEOPOTENTIAL_HEIGHT.P500HPA', 
        'GEOPOTENTIAL_HEIGHT.P850HPA','PRECIPITABLE_WATER.SFC']


#Past 30 days for the verification tool
end_date=datetime.now().date()-timedelta(days=1)
start_date=end_date-timedelta(days=30)
end=end_date.strftime('%Y-%m-%d')
start=start_date.strftime('%Y-%m-%d')


def get_df(location):
    if location=='Adelaide':
        df_ec, df_gfs, df_acg, df_acc, df_wrf=df_ec_a, df_gfs_a, df_acg_a, df_acc_a, df_wrf_a
    elif location=='Melbourne':
        df_ec, df_gfs, df_acg, df_acc, df_wrf=df_ec_m, df_gfs_m, df_acg_m, df_acc_m, df_wrf_m
    elif location=='Sydney':
        df_ec, df_gfs, df_acg, df_acc, df_wrf=df_ec_s, df_gfs_s, df_acg_s, df_acc_s, df_wrf_s
    elif location=='Bankstown':
        df_ec, df_gfs, df_acg, df_acc, df_wrf=df_ec_b, df_gfs_b, df_acg_b, df_acc_b, df_wrf_b
    elif location=='Canberra':
        df_ec, df_gfs, df_acg, df_acc, df_wrf=df_ec_c, df_gfs_c, df_acg_c, df_acc_c, df_wrf_c
    elif location=='Brisbane':
        df_ec, df_gfs, df_acg, df_acc, df_wrf=df_ec_br, df_gfs_br, df_acg_br, df_acc_br, df_wrf_br
    elif location=='Perth':
        df_ec, df_gfs, df_acg, df_acc, df_wrf=df_ec_p, df_gfs_p, df_acg_p, df_acc_p, df_wrf_p
    elif location=='Geraldton':
        df_ec, df_gfs, df_acg, df_acc, df_wrf=df_ec_g, df_gfs_g, df_acg_g, df_acc_g, df_wrf_g
    elif location=='Bunbury':
        df_ec, df_gfs, df_acg, df_acc, df_wrf=df_ec_bu, df_gfs_bu, df_acg_bu, df_acc_bu, df_wrf_bu
    elif location=='Albany':
        df_ec, df_gfs, df_acg, df_acc, df_wrf=df_ec_al, df_gfs_al, df_acg_al, df_acc_al, df_wrf_al
    elif location=='Kalgoorlie':
        df_ec, df_gfs, df_acg, df_acc, df_wrf=df_ec_k, df_gfs_k, df_acg_k, df_acc_k, df_wrf_k
    return df_ec, df_gfs, df_acg, df_acc, df_wrf


def get_obs(api_url):
    ### Get past 24 hour obs dataframe from weatherzone obs api
    ###
    response = requests.get(api_url)
    dictionary = response.json()
    location=dictionary['countries'][0]['locations'][0]['name']
    values=dictionary['countries'][0]['locations'][0]['historical_observation']
    df=pd.DataFrame(values)
    df.index=pd.to_datetime(df['local_time'])
    
    cols = ['temperature', 'feels_like', 'dew_point',
       'relative_humidity', 'wind_direction', 'wind_direction_compass',
       'wind_speed', 'wind_gust', 'avg_wind_speed_last_10_minutes',
       'max_wind_gust_last_10_minutes', 'rainfall_since_9am', 'pressure']

    return df[cols], location

obs_variables=['temperature', 'feels_like', 'dew_point', 'relative_humidity',
       'wind_direction', 'wind_direction_compass', 'wind_speed', 'wind_gust',
       'avg_wind_speed_last_10_minutes', 'max_wind_gust_last_10_minutes',
       'rainfall_since_9am', 'pressure']


app=dash.Dash()

app.layout = html.Div([
    html.H1('NEM and WEM forecasting', style={'textAlign':'center'}),
    
    html.H2('Multi model comparisons'),
    
    html.Div([
        html.Label('City: '),
        dcc.Dropdown(id='location-selection',
                options=[{'label':i, 'value':i } for i in nem_and_wem],
                value='Adelaide')],
            style={'width':'25%','display':'inline-block'}),
    html.Div([
        html.Label('Surface based variable: '),
        dcc.Dropdown(id='surface-selection',
                options=[{'label':i, 'value':i } for i in sfc_var],
                value='TEMPERATURE_MAX_DAY.H2M')],
                style={'width':'25%','display':'inline-block'}),
    html.Div([
        html.Label('Higher level variable: '),
        dcc.Dropdown(id='upper-selection',
                options=[{'label':i, 'value':i } for i in up_var],
                value='TEMPERATURE.P850HPA')],
                style={'width':'25%','display':'inline-block'}),
    html.Div([
        html.Label('Days: '),
        dcc.RangeSlider(id='date-select',min=0, max=14,step=1,value=[0,9]),
        ], style={'width':'25%','display':'inline-block', 'verticalAlign':'top'}),
    
    dcc.Graph(id='surface-graph'),
    dcc.Graph(id='upper-graph'),

    html.H2('Model details and ECMWF meteogram links'),
    
    html.Div([
        html.H3('Model run times:'),
        html.P(ec_date),
        html.P(gfs_date),
        html.P(ag_date),
        html.P(acc_date),
        html.P(wrf_date),
        ], style={'width': '42%', 'display': 'inline-block', 'vertical-align':'top'}),
    html.Div([
        dcc.Markdown('''
        ### ECMWF meteograms NEM
        * [Adelaide (23000)](https://charts.ecmwf.int/products/opencharts_meteogram?epsgram=classical_15d_with_climate&lat=-34.9333&lon=138.6&station_name=Adelaide)
        * [Melbourne (86338)](https://charts.ecmwf.int/products/opencharts_meteogram?epsgram=classical_15d_with_climate&lat=-37.814&lon=144.963&station_name=Melbourne)
        * [Sydney (66214)](https://charts.ecmwf.int/products/opencharts_meteogram?epsgram=classical_15d_with_climate&lat=-33.8679&lon=151.207&station_name=Sydney)
        * [Bankstown (66137)](https://charts.ecmwf.int/products/opencharts_meteogram?epsgram=classical_15d_with_climate&lat=-33.9167&lon=151.033&station_name=Bankstown)
        * [Canberra (70351)](https://charts.ecmwf.int/products/opencharts_meteogram?epsgram=classical_15d_with_climate&lat=-35.2835&lon=149.128&station_name=Canberra)
        * [Brisbane (40913)](https://charts.ecmwf.int/products/opencharts_meteogram?epsgram=classical_15d_with_climate&lat=-27.4679&lon=153.028&station_name=Brisbane)
        ''', link_target='_blank')], style={'width': '28%', 'display': 'inline-block', 'vertical-align':'top'}),
    html.Div([
        dcc.Markdown('''
        ### ECMWF meteograms WEM
        * [Perth (9225)](https://charts.ecmwf.int/products/opencharts_meteogram?epsgram=classical_15d_with_climate&lat=-31.9333&lon=115.833&station_name=Perth)
        * [Geraldton (8315)](https://charts.ecmwf.int/products/opencharts_meteogram?epsgram=classical_15d_with_climate&lat=-28.7667&lon=114.6&station_name=Geraldton)
        * [Bunbury (9965)](https://charts.ecmwf.int/products/opencharts_meteogram?epsgram=classical_15d_with_climate&lat=-33.3333&lon=115.633&station_name=Bunbury)
        * [Albany (9999)](https://charts.ecmwf.int/products/opencharts_meteogram?epsgram=classical_15d_with_climate&lat=-35.0169&lon=117.892&station_name=Albany)
        * [Kalgoorlie (12038)](https://charts.ecmwf.int/products/opencharts_meteogram?epsgram=classical_15d_with_climate&lat=-30.75&lon=121.467&station_name=Kalgoorlie)
        ''', link_target='_blank')], style={'width': '28%', 'display': 'inline-block', 'vertical-align':'top'}),

    html.H2('10 minute observations during the past 24 hours'),
    html.Div([
        html.Label('Enter BOM code: '),
        dcc.Input(id='input-code',
                  value=23000),
        ], style={'display':'inline-block', 'width':'25%', 'verticalAlign':'top'}),
    html.Div([
        html.Label('Choose a variable: '),], style={'display':'inline-block', 'width':'10%', 'verticalAlign':'top'}),
    html.Div([
        dcc.Dropdown(id='obs-variables',
                options=[{'label':i, 'value':i} for i in obs_variables],
                value='temperature'),
        ], style={'display':'inline-block', 'width':'15%', 'verticalAlign':'top'}),
    html.Div([
        html.Button(id='obs-button', children='Submit', n_clicks=0),
        ], style={'display':'inline-block', 'width':'20%', 'verticalAlign':'top', 'padding-left':'5%'}),
    dcc.Graph(id='plot-obs'),

    html.H2('Other useful observations'),
    
    html.Div([
        dcc.Markdown('''
        ### NEM aerological diagrams
        * [Adelaide](http://www.bom.gov.au/fwo/aviation/IDS65024/IDS65024.94672.png)
        * [Melbourne](http://www.bom.gov.au/fwo/aviation/IDS65024/IDS65024.94866.png)
        * [Sydney](http://www.bom.gov.au/fwo/aviation/IDS65024/IDS65024.94767.png)
        * [Williamtown](http://www.bom.gov.au/fwo/aviation/IDS65024/IDS65024.94776.png)
        * [Wagga](http://www.bom.gov.au/fwo/aviation/IDS65024/IDS65024.94910.png)
        * [Brisbane](http://www.bom.gov.au/fwo/aviation/IDS65024/IDS65024.94578.png)
        ''', link_target='_blank')], style={'width': '25%', 'display': 'inline-block', 'vertical-align':'top'}),
    html.Div([
        dcc.Markdown('''
        ### WEM aerological diagrams
        * [Perth](http://www.bom.gov.au/fwo/aviation/IDS65024/IDS65024.94610.png)
        * [Geraldton](http://www.bom.gov.au/fwo/aviation/IDS65024/IDS65024.94403.png)
        * [Albany](http://www.bom.gov.au/fwo/aviation/IDS65024/IDS65024.94802.png)
        * [Kalgoorlie](http://www.bom.gov.au/fwo/aviation/IDS65024/IDS65024.94637.png)
        ''', link_target='_blank')], style={'width': '25%', 'display': 'inline-block', 'vertical-align':'top'}),
    html.Div([
        dcc.Markdown('''
        ### SST analysis
        * [South Australia](http://www.bom.gov.au/products/IDYOC052.SA.SSTAnalysis.shtml)
        * [Victoria](http://www.bom.gov.au/products/IDYOC052.SE_Aust.SSTAnalysis.shtml)
        * [New South Wales](http://www.bom.gov.au/products/IDYOC052.NSW.SSTAnalysis.shtml)
        * [Queensland](http://www.bom.gov.au/products/IDYOC052.QLD.SSTAnalysis.shtml)
        * [Western Australia](http://www.bom.gov.au/products/IDYOC052.SW_WA.SSTAnalysis.shtml)
    ''', link_target='_blank')], style={'width': '25%', 'display': 'inline-block', 'vertical-align':'top'}),
    html.Div([
        dcc.Markdown('''
        ### SST anomaly
        * [South Australia](http://www.bom.gov.au/products/IDYOC052.SA.SSTAnomaly.shtml)
        * [Victoria](http://www.bom.gov.au/products/IDYOC052.SE_Aust.SSTAnomaly.shtml)
        * [New South Wales](http://www.bom.gov.au/products/IDYOC052.NSW.SSTAnomaly.shtml)
        * [Queensland](http://www.bom.gov.au/products/IDYOC052.QLD.SSTAnomaly.shtml)
        * [Western Australia](http://www.bom.gov.au/products/IDYOC052.SW_WA.SSTAnomaly.shtml)
        ''', link_target='_blank')], style={'width': '25%', 'display': 'inline-block', 'vertical-align':'top'}),

    html.H2('Verification tool past 30 days'),
    
    html.Div([
        dcc.Markdown(f'''
        ### NEM max temp
        * [Adelaide](http://internal.theweather.com.au/intranet/ops/fcast/verification/index.php?loc_code=112150&field=max_temp&start_date={start}&end_date={end}&issue_time=)
        * [Melbourne](http://internal.theweather.com.au/intranet/ops/fcast/verification/index.php?loc_code=9477&field=max_temp&start_date={start}&end_date={end}&issue_time=)
        * [Sydney](http://internal.theweather.com.au/intranet/ops/fcast/verification/index.php?loc_code=9770&field=max_temp&start_date={start}&end_date={end}&issue_time=)
        * [Bankstown](http://internal.theweather.com.au/intranet/ops/fcast/verification/index.php?loc_code=8939&field=max_temp&start_date={start}&end_date={end}&issue_time=)
        * [Canberra](http://internal.theweather.com.au/intranet/ops/fcast/verification/index.php?loc_code=9028&field=max_temp&start_date={start}&end_date={end}&issue_time=)
        * [Brisbane](http://internal.theweather.com.au/intranet/ops/fcast/verification/index.php?loc_code=9250&field=max_temp&start_date={start}&end_date={end}&issue_time=)
        ''', link_target='_blank')], style={'width': '24%', 'display': 'inline-block', 'vertical-align':'top'}),
    html.Div([
        dcc.Markdown(f'''
        ### NEM min temp
        * [Adelaide](http://internal.theweather.com.au/intranet/ops/fcast/verification/index.php?loc_code=112150&field=min_temp&start_date={start}&end_date={end}&issue_time=)
        * [Melbourne](http://internal.theweather.com.au/intranet/ops/fcast/verification/index.php?loc_code=9477&field=min_temp&start_date={start}&end_date={end}&issue_time=)
        * [Sydney](http://internal.theweather.com.au/intranet/ops/fcast/verification/index.php?loc_code=9770&field=min_temp&start_date={start}&end_date={end}&issue_time=)
        * [Bankstown](http://internal.theweather.com.au/intranet/ops/fcast/verification/index.php?loc_code=8939&field=min_temp&start_date={start}&end_date={end}&issue_time=)
        * [Canberra](http://internal.theweather.com.au/intranet/ops/fcast/verification/index.php?loc_code=9028&field=min_temp&start_date={start}&end_date={end}&issue_time=)
        * [Brisbane](http://internal.theweather.com.au/intranet/ops/fcast/verification/index.php?loc_code=9250&field=min_temp&start_date={start}&end_date={end}&issue_time=)
        ''', link_target='_blank')], style={'width': '24%', 'display': 'inline-block', 'vertical-align':'top'}),
    html.Div([
        dcc.Markdown(f'''
        ### WEM max temp
        * [Perth](http://internal.theweather.com.au/intranet/ops/fcast/verification/index.php?loc_code=9528&field=max_temp&start_date={start}&end_date={end}&issue_time=)
        * [Geraldton](http://internal.theweather.com.au/intranet/ops/fcast/verification/index.php?loc_code=9531&field=max_temp&start_date={start}&end_date={end}&issue_time=)
        * [Bunbury](http://internal.theweather.com.au/intranet/ops/fcast/verification/index.php?loc_code=9563&field=max_temp&start_date={start}&end_date={end}&issue_time=)
        * [Albany](http://internal.theweather.com.au/intranet/ops/fcast/verification/index.php?loc_code=9541&field=max_temp&start_date={start}&end_date={end}&issue_time=)
        * [Kalgoorlie](http://internal.theweather.com.au/intranet/ops/fcast/verification/index.php?loc_code=9532&field=max_temp&start_date={start}&end_date={end}&issue_time=)
        ''', link_target='_blank')], style={'width': '24%', 'display': 'inline-block', 'vertical-align':'top'}),
    html.Div([
        dcc.Markdown(f'''
        ### WEM min temp
        * [Perth](http://internal.theweather.com.au/intranet/ops/fcast/verification/index.php?loc_code=9528&field=min_temp&start_date={start}&end_date={end}&issue_time=)
        * [Geraldton](http://internal.theweather.com.au/intranet/ops/fcast/verification/index.php?loc_code=9531&field=min_temp&start_date={start}&end_date={end}&issue_time=)
        * [Bunbury](http://internal.theweather.com.au/intranet/ops/fcast/verification/index.php?loc_code=9563&field=min_temp&start_date={start}&end_date={end}&issue_time=)
        * [Albany](http://internal.theweather.com.au/intranet/ops/fcast/verification/index.php?loc_code=9541&field=min_temp&start_date={start}&end_date={end}&issue_time=)
        * [Kalgoorlie](http://internal.theweather.com.au/intranet/ops/fcast/verification/index.php?loc_code=9532&field=min_temp&start_date={start}&end_date={end}&issue_time=)
        ''', link_target='_blank')], style={'width': '24%', 'display': 'inline-block', 'vertical-align':'top'}),
    
    html.H3('* Note about 850hpa plots'),
    html.P('The horizontal lines that appear in the 850hPa plots are median values for past extreme days.'),
    html.P('Between May and September the line in the 850 T plot is for evaluating cold maxima and the line in the 850 Td is for evaluating cold minima.'),
    html.P('Between October and April two lines appear in the 850 T plot one for evaluating hot maxima (red line) and the other for hot minima (orange line).')
])

@app.callback(Output('surface-graph', 'figure'),
             [Input('location-selection','value'), Input('surface-selection','value'),
              Input('date-select','value')])
def surface_plot(location, variable, start_end_dates):
    df_ec, df_gfs, df_acg, df_acc, df_wrf = get_df(location)

    dates=np.unique(df_gfs_a.index.date)
    start_date=dates[start_end_dates[0]]
    end_date=dates[start_end_dates[1]]
    
    df_ec = df_ec[(df_ec.index.date<=end_date) & (df_ec.index.date>=start_date)]
    df_acg = df_acg[(df_acg.index.date<=end_date) & (df_acg.index.date>=start_date)]
    df_gfs = df_gfs[(df_gfs.index.date<=end_date) & (df_gfs.index.date>=start_date)]
    df_acc = df_acc[(df_acc.index.date<=end_date) & (df_acc.index.date>=start_date)]
    df_wrf = df_wrf[(df_wrf.index.date<=end_date) & (df_wrf.index.date>=start_date)]

    if variable=='PRECIPITATION_3H.SFC':
        gfs_values=df_gfs[variable].dropna()
        acg_values=df_acg[variable].dropna()
        acc_values=df_acc[variable].dropna()
        wrf_values=df_wrf[variable].dropna()
    else:
        ec_values=df_ec[variable].dropna()
        gfs_values=df_gfs[variable].dropna()
        acg_values=df_acg[variable].dropna()
        acc_values=df_acc[variable].dropna()
        wrf_values=df_wrf[variable].dropna()   
        
    if variable in ['TEMPERATURE_MAX_DAY.H2M', 'TEMPERATURE_MIN_DAY.H2M','PRECIPITATION_DAY.SFC' ]:
        data=[go.Bar(x=ec_values.index, y=ec_values, name='EC'),
          go.Bar(x=acg_values.index, y=acg_values, name='ACG'),
          go.Bar(x=gfs_values.index, y=gfs_values, name='GFS'),
        go.Bar(x=wrf_values.index, y=wrf_values, name='WRF'),
            go.Bar(x=acc_values.index, y=acc_values, name='ACC'),
         ]
    elif variable=='PRECIPITATION_3H.SFC':  
        data=[go.Scatter(x=acg_values.index, y=acg_values, name='ACG'),
          go.Scatter(x=gfs_values.index, y=gfs_values, name='GFS'),
        go.Scatter(x=wrf_values.index, y=wrf_values, name='WRF'),
            go.Scatter(x=acc_values.index, y=acc_values, name='ACC'),
         ]
    else:
        data=[go.Scatter(x=ec_values.index, y=ec_values, name='EC'),
          go.Scatter(x=acg_values.index, y=acg_values, name='ACG'),
          go.Scatter(x=gfs_values.index, y=gfs_values, name='GFS'),
        go.Scatter(x=wrf_values.index, y=wrf_values, name='WRF'),
            go.Scatter(x=acc_values.index, y=acc_values, name='ACC'),
         ]
    layout=go.Layout(title=f'Forecast {variable} at {location}')
    figure=go.Figure(data=data, layout=layout)

    cur_month=datetime.now().month
    if variable == 'TEMPERATURE_MAX_DAY.H2M' or variable =='TEMPERATURE.H2M':
        if  cur_month>=5 and cur_month<=9:
            figure.add_hline(crit_values.loc[location, 'C_max'], line_color='blue')
        else:
            figure.add_hline(crit_values.loc[location, 'H_max'],line_color='red')

    if variable == 'TEMPERATURE_MIN_DAY.H2M' or variable =='TEMPERATURE.H2M' or variable =='DEW_POINT.H2M':
        if  cur_month>=5 and cur_month<=9:
            figure.add_hline(crit_values.loc[location, 'C_min'],line_color='blue')
        else:
            figure.add_hline(crit_values.loc[location, 'H_min'],line_color='red')

    return figure

@app.callback(Output('upper-graph', 'figure'),
             [Input('location-selection','value'), Input('upper-selection','value'),
             Input('date-select','value')])
def upper_plot(location, variable, start_end_dates):
    df_ec, df_gfs, df_acg, df_acc, df_wrf = get_df(location)
    
    dates=np.unique(df_gfs_a.index.date)
    start_date=dates[start_end_dates[0]]
    end_date=dates[start_end_dates[1]]
    
    df_ec = df_ec[(df_ec.index.date<=end_date) & (df_ec.index.date>=start_date)]
    df_acg = df_acg[(df_acg.index.date<=end_date) & (df_acg.index.date>=start_date)]
    df_gfs = df_gfs[(df_gfs.index.date<=end_date) & (df_gfs.index.date>=start_date)]
    df_acc = df_acc[(df_acc.index.date<=end_date) & (df_acc.index.date>=start_date)]
    df_wrf = df_wrf[(df_wrf.index.date<=end_date) & (df_wrf.index.date>=start_date)]
    
    ec_values=df_ec[variable].dropna()
    gfs_values=df_gfs[variable].dropna()
    acg_values=df_acg[variable].dropna()
    acc_values=df_acc[variable].dropna()
    wrf_values=df_wrf[variable].dropna()   

    data=[go.Scatter(x=ec_values.index, y=ec_values, name='EC'),
              go.Scatter(x=acg_values.index, y=acg_values, name='ACG'),
              go.Scatter(x=gfs_values.index, y=gfs_values, name='GFS'),
          go.Scatter(x=wrf_values.index, y=wrf_values, name='WRF'),
          go.Scatter(x=acc_values.index, y=acc_values, name='ACC'),
             ]
    layout=go.Layout(title=f'Forecast {variable} at {location}')
    figure=go.Figure(data=data, layout=layout)

    cur_month=datetime.now().month
    if variable == 'TEMPERATURE.P850HPA':
        if  cur_month>=5 and cur_month<=9:
            figure.add_hline(crit_values.loc[location, '850_cmax'], line_color='blue')
        else:
            figure.add_hline(crit_values.loc[location, '850_hmax'], line_color='red')
            figure.add_hline(crit_values.loc[location, '850_hmin'], line_color='orange')

    if variable == 'DEWPOINT.P850HPA':
        if  cur_month>=5 and cur_month<=9:
            figure.add_hline(crit_values.loc[location, '850_cmin'], line_color='blue')

    return figure


@app.callback(Output('plot-obs', 'figure'),
            [Input('obs-button', 'n_clicks')], 
            [State('input-code', 'value'), State('obs-variables', 'value')])
def obs_plot(n_clicks, loc, variable):
    url=f'https://ws.weatherzone.com.au/?lt=site&lc={loc}&locdet=1&histobs=1(interval=1min,order=asc)&wb=1&fdi=1&format=json&u=1'
    df, town = get_obs(url)
    if variable =='temperature':
        data=[go.Scatter(x=df.index, y=df['dew_point'], mode='lines', name='dewpoint'),
            go.Scatter(x=df.index, y=df[variable], mode='lines', name=variable)]
        layout=go.Layout({'title':f'10 minute {variable} and dewpoint for {town} ({loc}) in the past 24 hours'})
    elif variable=='feels_like':
        data=[go.Scatter(x=df.index, y=df[variable], mode='lines', name=variable, line_color='black'),
            go.Scatter(x=df.index, y=df['temperature'], mode='lines', name='temperature', line_color='red')]
        layout=go.Layout({'title':f'10 minute {variable} temperature for {town} ({loc}) in the past 24 hours'})
    else:
        data=[go.Scatter(x=df.index, y=df[variable], mode='lines')]
        layout=go.Layout({'title':f'10 minute {variable} for {town} ({loc}) in the past 24 hours'})
    return go.Figure(data=data, layout=layout)


if __name__=='__main__':
    app.run(debug=False, host='0.0.0.0', port=8050)

